## Better Free ##

There are established best practices around the utilization of the free function to deallocate memory such as:
   * Checking if the pointer passed is NULL
   * Setting the pointer to NULL after deallocation

better_free() handles both of these operations while utilizing free(). 

#### Usage ####

Define the following macro in the program in which you are using better_free(). If not you will receive compiler
warnings due to the need for the pointer to be explicitly
cast to void.

``#define better_free(p) better_free((void**) &(p))
``

This macro will eliminate the need to manually perform any casting.
